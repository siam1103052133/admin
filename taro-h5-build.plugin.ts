import { IPluginContext } from '@tarojs/service';
import path from 'path';

type PluginOptions = {
  assetPath: string;
  htmlName: string;
  publicPath: string;
};

export default function H5BuildPlugin(
  ctx: IPluginContext,
  pluginOpts: PluginOptions = {
    assetPath: '',
    htmlName: 'index.html',
    publicPath: 'https://cdn.wakecloud.com/resources/dss-web-portal/',
  }
) {
  if (process.env.NODE_ENV === 'production' && process.env.TARO_ENV === 'h5') {
    // 判断h5环境与是否生产环境
    function assetPath(p) {
      return path.posix.join(pluginOpts.assetPath, p);
    }

    const h5Config = ctx.initialConfig.h5;

    ctx.initialConfig.h5 = {
      ...h5Config,
      output: {
        ...(h5Config?.output || {}),
        filename: assetPath('js/[name].[hash:8].js'),
        chunkFilename: assetPath('js/[name].[chunkhash:8].js'),
      },
      miniCssExtractPluginOption: {
        ...(h5Config?.miniCssExtractPluginOption || {}),
        filename: assetPath('css/[name].css'),
        chunkFilename: assetPath('css/[id].css'),
      },
      chunkDirectory: assetPath('chunk'),
      staticDirectory: assetPath('static'),
      publicPath: pluginOpts.publicPath,
      webpackChain(config) {
        // 修改生成的html名称
        config.plugin('htmlWebpackPlugin').init((Plugin, args) => {
          args[0].filename = pluginOpts.htmlName;
          return new Plugin(...args);
        });
      },
    };
  }
}
