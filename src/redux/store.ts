
type ReducerAction = {
    type: string
    payload: any
}
type ReduxReducer<T = any> = (initialState: T, action: ReducerAction) => T
const randomString = () =>
    Math.random().toString(36).substring(7).split('').join('.')
const DEFAULT_ACTION = {
    type: `@DEFAULT${randomString()}`,
    payload: null
}

class Store<T = Record<string, any>> {
    reducer: ReduxReducer<T>
    // prevState?: T
    currentState?: T
    // nextAction: ReducerAction = DEFAULT_ACTION
    constructor(reducer: ReduxReducer<T>, preloadedState?: T) {
        this.reducer = reducer
        if (preloadedState) {
            this.currentState = preloadedState
        }
        this.dispatch(DEFAULT_ACTION)
    }
    getState() {
        return this.currentState
    }
    dispatch(action: ReducerAction) {
        this.currentState = this.reducer(this.currentState || {} as T, action)
    }
}

export function createStore<T>(reducer: ReduxReducer<T>, preloadedState: T) {
    return new Store<T>(reducer, preloadedState)
}

function todos(state: string[] = [], action: ReducerAction) {
    switch (action.type) {
        case 'ADD_TODO':
            return state.concat([action.payload])
        default:
            return state
    }
}

export const store = createStore(todos, ['Use Redux'])


