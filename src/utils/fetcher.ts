export interface ResponseType<T> {
  data: T;
}

export default function fetcher(requestInit: RequestInit['method'] | RequestInit) {
  return (url: string) => {
    let req: RequestInit = {};

    if (typeof requestInit === 'string') {
      req.method = requestInit;
    } else if (requestInit) {
      req = requestInit;
    }
    return fetch(url, req).then((data) => data.json());
  };
}
