import { ResponseType } from './fetcher';
export interface QueryPage {
  pageNo: number;
  pageSize: number;
}
export interface DoubanItem {
  Id: number;
  Url: string;
  Name: string;
  Author: string;
  Score: number;
  Library: string;
  Cover: string;
}
type ResponseTypes = {
  '/api/list/douban': ResponseType<DoubanItem[]>;
};
export default ResponseTypes;
