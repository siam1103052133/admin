import React, { createElement, FC } from 'react';
import { Link } from 'react-router-dom';
import { Menu } from 'antd';
import { PlayCircleOutlined, UnorderedListOutlined, LineChartOutlined, LayoutFilled } from '@ant-design/icons';
import './Layout.css';
import Footer from './Footer'
const Layout: FC<{}> = (props) => {
    const menus = [
        {
            label: 'list',
            icon: UnorderedListOutlined,
            path: '/'
        },
        {
            label: 'player',
            icon: PlayCircleOutlined,
            path: '/player'
        },
        {
            label: 'upload',
            icon: LineChartOutlined,
            path: '/upload'
        },
        {
            label: 'demo',
            icon: LayoutFilled,
            path: '/demo'
        }
    ]
    return (
        <div className="App height-inherit">
            <aside>
                <Menu
                    className="height-100"
                    style={{ width: 256 }}
                    defaultSelectedKeys={[menus[0].label]}
                    theme="dark"
                    mode="inline"
                >
                    {
                        menus.map(it => (
                            <Menu.Item icon={createElement(it.icon)} key={it.label}>
                                <Link to={it.path} >{it.label}</Link>
                            </Menu.Item>
                        ))
                    }
                </Menu>
            </aside>
            <main className="App-main">
                {props.children}
                <Footer />
            </main>
        </div>
    );
}

export default Layout;
