import { GithubOutlined } from '@ant-design/icons'
import './Footer.css'

function Footer() {
    return (<div>
        <GithubOutlined className="icon" />
        <a rel="noreferrer" target="_blank" href="https://github.com/siam-ese">https://github.com/siam-ese</a>
    </div>)
}

export default Footer