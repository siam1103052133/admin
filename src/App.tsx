import React from 'react';

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import './App.css'
import Layout from '@/Layout/Layout'
import Home from '@/views/Home/Home'
import Player from './views/Player/Player';
import Upload from './views/Upload/Upload';
import Demo from './views/Demo/Demo'
function App() {
  return (
    <Router>
      <Layout>
        <Switch>
          <Route path="/player">
            <Player />
          </Route>
          <Route path="/upload">
            <Upload />
          </Route>
          <Route path="/Demo">
            <Demo />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Layout>
    </Router>
  );
}

export default App;
