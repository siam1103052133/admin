
import React, { useCallback, useEffect, useRef, useState } from 'react'

export type InfiniteScrollerProps = {
    loadMore: Function
    loader: Element | JSX.Element
    threshold: number
    hasMore?: boolean
    pageStart?: number
    useWindow?: boolean
    getParentElement?: () => HTMLElement
    initialLoad?: boolean
}

const InfiniteScroller: React.FC<InfiniteScrollerProps> = (props) => {
    const {
        getParentElement,
        useWindow,
        threshold,
        loadMore,
        initialLoad
    } = props
    let scorllEl = useRef<HTMLDivElement>()
    let pageStart = props.pageStart || 0

    const [loading, setLoading] = useState(false)

    const getParentNode = useCallback((el: typeof scorllEl.current) => {
        const scrollElement = getParentElement?.()
        if (scrollElement) {
            return scrollElement
        }

        return (el && el.parentElement)
    }, [getParentElement])

    function calculateTopPosition(el: HTMLElement | null): number {
        if (!el) return 0

        return el.offsetTop + calculateTopPosition(el.offsetParent as HTMLElement)
    }
    function calculateOffset(el: HTMLElement | null, scrollTop: number) {
        if (!el) return 0

        return calculateTopPosition(el) + (el.offsetHeight - scrollTop - window.innerHeight)
    }


    const scrollListener = useCallback((e?: Event) => {
        const node = scorllEl.current
        const parentNode = getParentNode(node)

        if (!node) return

        let offset: number
        if (props.useWindow) {
            const doc = document.documentElement || document.body.parentElement || document.body // 全局滚动容器
            const scrollTop = window.pageYOffset || doc.scrollTop // 全局的 "scrollTop"

            offset = calculateOffset(node, scrollTop)
        } else {
            if (!parentNode) return
            offset = node.scrollHeight - parentNode.scrollTop - parentNode.clientHeight
        }

        if (offset < props.threshold) {
            node.removeEventListener('scroll', scrollListener)
            props.loadMore(pageStart)
            setLoading(true)
        }
    }, [useWindow, threshold, loadMore])

    const attachListener = useCallback((el: HTMLElement | Window) => {
        el.addEventListener('scroll', scrollListener, {
            passive: true
        })
        el.addEventListener('resize', scrollListener, {
            passive: true
        })
        if (initialLoad) {
            scrollListener()
        }
    }, [initialLoad, scrollListener])

    function detachListener(el: HTMLElement | Window) {
        el.addEventListener('scroll', scrollListener)
        el.addEventListener('resize', scrollListener)
    }



    useEffect(() => {
        const el = getParentNode(scorllEl.current)
        const scrollEl = props.useWindow ? window : el

        scrollEl && attachListener(scrollEl)
        return () => {
            scrollEl && detachListener(scrollEl)
        }
    }, [scorllEl])

    return (
        <div ref={n => n && (scorllEl.current = n)}>
            {props.children}
            {loading && props.loader}
        </div>
    )
}
export default InfiniteScroller