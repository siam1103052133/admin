import Button from "../Button";

describe('Button', () => {
    it('renders correctly', () => {
        expect(<Button>Follow</Button>).toMatchSnapshot()
    })
})