import { Button as AntdButton } from 'antd'
import { FC } from 'react'
const Button: FC = (props) => {
    return <AntdButton>{props.children}</AntdButton>
}

export default Button