import React, { useCallback, useEffect } from 'react'

export type ContentEditableProps = {
    value: string,
    innerRef?: React.RefObject<HTMLDivElement> | ((n: HTMLDivElement) => void)
    html?: string
    disabled?: boolean
    onChange: (val: string) => void
}

const ContentEditable: React.FC<ContentEditableProps> = (props) => {
    const { innerRef, value, onChange } = props
    let lastHtml: string = props.value
    let element: HTMLDivElement

    function getEl() {
        return (innerRef && typeof innerRef !== 'function') ? innerRef.current : element
    }

    const updateContentEditable = (el: HTMLDivElement | null) => {
        if (!el) return
        el.contentEditable = props.disabled ? 'false' : 'true'
    }

    useEffect(() => {
        const el = getEl()

        if (!el) return
        const cursor = document.createTextNode('')
        if (!cursor) return
        el.appendChild(cursor)

        // 判断是否选中
        const isFocused = document.activeElement === el
        if (!cursor || cursor.nodeValue === null || !isFocused) return

        // 将光标放到最后
        const selection = window.getSelection()
        if (selection !== null) {
            const range = document.createRange()
            range.setStart(cursor, cursor.nodeValue.length)
            range.collapse(true)

            selection.removeAllRanges()
            selection.addRange(range)
        }

        // 重新 focus
        if (el instanceof HTMLElement) el.focus()
    }, [props.value])

    useEffect(() => {
        updateContentEditable(getEl())
    }, [updateContentEditable])

    function handleInput(e: React.FormEvent<HTMLDivElement>) {
        const el = (e.target as HTMLDivElement)
        if (onChange && lastHtml !== el.innerHTML) {
            lastHtml = el.innerHTML
            console.log('last', lastHtml)
            onChange(lastHtml)
        }
    }

    return (<div
        style={{ height: 'inherit' }}
        ref={(!innerRef || typeof innerRef === 'function') ? ((n) => {
            if (n) {
                element = n
                updateContentEditable(n)
                innerRef && innerRef(n)
            }
        }) : innerRef}
        onInput={handleInput}
        dangerouslySetInnerHTML={{ __html: value || '' }}
    ></div>)
}

export default ContentEditable