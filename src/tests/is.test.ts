import * as is from '../packages/is';

test('is type', () => {
  expect(is.array([])).toBe(true);
  expect(is.object({})).toBe(true);
  expect(is.set(new Set())).toBe(true);
  expect(is.map(new Map())).toBe(true);
});
