import produce from '../packages/immer';

test('produce example', () => {
  const baseState = [
    {
      title: 'Learn TypeScript',
      done: true,
    },
    {
      title: 'Try Immer',
      done: false,
    },
  ];

  const nextState = produce(baseState, (draft) => {
    draft.push({ title: 'Tweet about it', done: false });
    draft[1].done = true;
  });

  expect(baseState[1].done).toBe(false);
  expect(nextState[1].done).toBe(true);
  expect(nextState[2]).toEqual({ title: 'Tweet about it', done: false });
});

test('produce curry', () => {
  const curryProduce = produce<any[]>((draft) => {
    draft[1] = 'aaaa';
  });
  const list = [1, 2, 3];
  const nextList = curryProduce(list);

  expect(list[1]).toBe(2);
  expect(nextList[1]).toBe('aaaa');
});
