export const toString = (value: any) => Object.prototype.toString.call(value);

const typeStringMap = {
  object: toString({}),
  array: toString([]),
  map: toString(new Map()),
  set: toString(new Set()),
};

export function object(obj: any): obj is object {
  return toString(obj) === typeStringMap.object;
}

export function map(obj: any): obj is Map<any, any> {
  return toString(obj) === typeStringMap.map;
}

export function set(obj: any): obj is Set<any> {
  return toString(obj) === typeStringMap.set;
}

export function array(obj: any): obj is any[] {
  return toString(obj) === typeStringMap.array;
}
