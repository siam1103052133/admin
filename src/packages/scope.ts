import type { Draftable } from './immer';

export type Scope = {
  drafts: Draftable[];
  parent?: Scope;
  canAutoFreeze: boolean;
  unfinalizedDrafts: number;
};

let currentScope: Scope;
export function getCurrentScope() {
  return currentScope;
}
function create(parent: Scope): Scope {
  return {
    drafts: [],
    parent,
    canAutoFreeze: true,
    unfinalizedDrafts: 0,
  };
}

function enter() {
  return (currentScope = create(currentScope!));
}

function leave() {}

function revoke() {}

const scopeMethods = {
  create,
  enter,
  leave,
  revoke,
};

export default scopeMethods;
