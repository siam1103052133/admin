import * as is from './is';
import { Scope, getCurrentScope } from './scope';
const hasSymbol = typeof Symbol !== 'undefined' && typeof Symbol('x') === 'symbol';

type PropKey = string | number | symbol;

const DRAFTABLE_SYMBOL: unique symbol = hasSymbol
  ? Symbol.for('__DRAFTABLE_SYMBOL__')
  : ('__DRAFTABLE_SYMBOL__' as any);

// Represents the object is drafted
const DRAFT_STATE_SYMBOL: unique symbol = hasSymbol
  ? Symbol.for('__DRAFT_STATE_SYMBOL__')
  : ('__DRAFT_STATE_SYMBOL__' as any);

export type Draftable = Set<any> | Map<any, any> | any[] | Record<PropKey, any>;

export type Drafted = {
  [DRAFT_STATE_SYMBOL]?: ProxyState;
} & Draftable;

function isDraftable(value: any): value is Draftable {
  if (!value) return false;
  return is.object(value) || is.array(value) || is.map(value) || is.set(value);
}

function produce<T extends Draftable>(
  producer: (draft: T, ...args: any[]) => void
): <T>(state: T, ...args: any[]) => T;
function produce<T>(state: T, recipe?: (draft: T) => void): T;
function produce(state: any, recipe?: any): any {
  if (typeof state === 'function') {
    const curryRecipe = state;
    return function curriedProduce<T>(state: T, ...args: any[]) {
      return produce(state, (draft) => curryRecipe(draft, ...args));
    };
  }
  if (isDraftable(state)) {
    const proxy = createProxy(state);
    if (recipe) {
      recipe(proxy);
    }
    return proxy;
  } else {
    return state;
  }
}

type ProxyResult<T> = T & { $revoke: () => void };
enum ProxyTypes {
  object = 1,
  array,
  set,
  map,
}
type ProxyState = {
  type: ProxyTypes;
  scope: Scope;
  parent?: ProxyState;
  copy?: Drafted;
  base: Drafted;
  draft?: Drafted;
  assigned: Record<string, boolean>;
  $revoke?: () => void;
};

function latest(x: ProxyState) {
  return x.copy || x.base;
}

function peek(draft: Drafted, p: PropKey) {
  const state = draft[DRAFT_STATE_SYMBOL];
  const source = state ? latest(state) : draft;

  return (source as Record<PropKey, any>)[p];
}

function shallowCopy(draft: Drafted): Drafted {
  if (is.array(draft)) {
    return draft.slice();
  }
  const value = { ...draft } as Drafted;
  delete value[DRAFT_STATE_SYMBOL];
  return value;
}

function prepareCopy(state: ProxyState) {
  if (!state.copy) {
    state.copy = shallowCopy(state.base);
  }
}

function getDescriptorFromProto(source: any, prop: string) {
  if (!(prop in source)) return undefined;
  let proto = Object.getPrototypeOf(source);
  while (proto) {
    const desc = Object.getOwnPropertyDescriptor(proto, prop);
    if (desc) return desc;
    proto = Object.getPrototypeOf(proto);
  }
  return undefined;
}

function createProxy<T extends Draftable>(base: T, parent?: ProxyState): ProxyResult<T> {
  const isArray = Array.isArray(base);
  const getTarget = (target: ProxyState | ProxyState[]): ProxyState =>
    isArray ? (target as ProxyState[])[0] : (target as ProxyState);

  const state: ProxyState = {
    type: isArray ? ProxyTypes.array : ProxyTypes.object,
    scope: parent ? parent.scope : getCurrentScope()!,
    base,
    parent,
    assigned: {},
  };

  const { revoke, proxy } = Proxy.revocable(isArray ? [state] : state, {
    get(target, p) {
      if (p === DRAFT_STATE_SYMBOL) return state;
      target = getTarget(target);
      const source = latest(target);

      const value = Reflect.get(source, p);
      if (!isDraftable(value)) {
        return value;
      }

      if (value === peek(target.base, p)) {
        prepareCopy(state);
        // @ts-ignore
        return (state.copy![p] = createProxy(value, state));
      }

      return value;
    },
    set(state: ProxyState, prop: string, value) {
      state = getTarget(state);
      const source = latest(state);
      const desc = getDescriptorFromProto(source, prop);
      if (desc?.set) {
        desc.set.call(state.draft!, value);
        return true;
      }
      if (!state.assigned[prop]) {
        prepareCopy(state);
      }
      // @ts-ignore
      state.copy![prop] = value;

      state.assigned[prop] = true;
      return true;
    },
  });
  state.draft = proxy;
  state.$revoke = revoke;
  //@ts-ignore
  return proxy;
}

export default produce;
