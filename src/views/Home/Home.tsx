import React, { useState } from 'react'
import useSWR from 'swr'
import { Card, Pagination } from 'antd'
import styles from './Home.module.scss'
import axios from 'axios'
import ResponseTypes from '@/utils/ResponseTypes'
// import { useLocation } from 'react-router'

function Home() {
    const [page, setPage] = useState({
        pageNo: 1,
        pageSize: 10
    })
    console.log(window.location)
    const { data: list } = useSWR<ResponseTypes['/api/list/douban']>(['/api/list/douban', page], (url) => axios.post(url, page).then(res => res.data))

    return (<div className={styles.HomePage}>
        {
            list?.data.map(item => {
                return (<Card hoverable
                    className={styles.card}
                    key={item.Id}
                    style={{ width: 220 }}
                    cover={<img className={styles.card__image} alt="example" src={item.Cover} />}>
                    <Card.Meta title={item.Name} />
                </Card>)
            })
        }
        <div className={styles.PageFooter}>
            <Pagination current={page.pageNo} pageSize={page.pageSize} onChange={(pageNo, pageSize) => {
                setPage({
                    pageNo,
                    pageSize: pageSize || page.pageSize
                })
            }} total={500} />
        </div>
    </div>)
}

export default Home