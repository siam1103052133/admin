import React, { FC, useEffect, useState } from "react"
import InfiniteScroller from "@/components/InfiniteScroller"
import ContentEditable from "@/components/Contenteditable"
import axios from 'axios'
import ResponseTypes, { DoubanItem, QueryPage } from '@/utils/ResponseTypes'
import { List, Avatar, Spin, Button, Input } from 'antd'
import KeepAlive from 'react-activation'
const Counter: FC<{ msg: string }> = (props) => {
    const [count, setCount] = useState(0)

    return <div>
        {count}
        msg: {props.msg}
        <Button onClick={() => setCount(c => c + 1)}>Add Count</Button>
    </div>
}


const ListPage = () => {
    const [page, setPage] = useState<QueryPage>({
        pageNo: 1,
        pageSize: 20
    })
    const [text, setText] = useState('77777')
    const [list, setList] = useState<DoubanItem[]>([])
    const [showCounter, setShowCounter] = useState(true)
    const [msg, setMsg] = useState('')
    function loadMore() {
        setPage({
            ...page,
            pageNo: page.pageNo + 1
        })
    }

    useEffect(() => {
        axios.post<ResponseTypes['/api/list/douban']>('/api/list/douban', page)
            .then(res => {
                if (res.data.data) {
                    setList(list.concat(res.data.data))
                }
            })
    }, [page])

    return (
        <div>
            <section>
                <Button onClick={() => setShowCounter(!showCounter)}>Toggle show Counter</Button>
                {showCounter && (
                    <KeepAlive>
                        <Counter msg={msg}></Counter>
                    </KeepAlive>)}
                <Input value={msg} onChange={v => setMsg(v.target.value)} placeholder="Please input msg" />
            </section>
            <section>
                <div style={{ maxHeight: '500px', overflow: 'scroll' }}>
                    <InfiniteScroller threshold={100} loadMore={loadMore} loader={<Spin />}>
                        {
                            list.map((item, i) => (
                                <List.Item key={`${item.Id}_${i}`}>
                                    <List.Item.Meta
                                        avatar={<Avatar src={item.Cover} />}
                                        title={item.Name}
                                        description={item.Author}
                                    />
                                </List.Item>
                            ))
                        }
                    </InfiniteScroller>
                </div>
            </section>
            <section style={{ height: '500px' }}>
                <ContentEditable value={text} onChange={(val) => setText(val)} />
            </section>
        </div>
    )

}

export default ListPage